#!/bin/bash
PATH=/usr/share/rvm/gems/ruby-3.0.2/bin:/usr/share/rvm/gems/ruby-3.0.2@global/bin:/usr/share/rvm/rubies/ruby-3.0.2/bin:/usr/share/rvm/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

cd /var/www/api
# Run migrations
RAILS_ENV=production bin/rails db:migrate
